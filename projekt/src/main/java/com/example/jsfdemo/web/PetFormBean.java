package com.example.jsfdemo.web;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIForm;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.event.ComponentSystemEvent;
import javax.faces.model.ListDataModel;
import javax.faces.validator.ValidatorException;
import javax.inject.Inject;
import javax.inject.Named;

import com.example.jsfdemo.domain.Pet;
import com.example.jsfdemo.service.PetManager;

@SessionScoped
@Named("petBean")
public class PetFormBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private Pet pet = new Pet();

	private ListDataModel<Pet> pets = new ListDataModel<Pet>();

	@Inject
	private PetManager pm;

	public Pet getPet() {
		return pet;
	}

	public void setPet(Pet pet) {
		this.pet = pet;
	}

	public ListDataModel<Pet> getAllPets() {
		pets.setWrappedData(pm.getAllPets());
		return pets;
	}

	// Actions
	public String addPet() {
		pm.addPet(pet);
		return "showPets";
		//return null;
	}

	public String deletePet() {
		Pet petToDelete = pets.getRowData();
		pm.deletePet(petToDelete);
		return null;
	}

	// Validators

	// Business logic validation
	public void uniqueNumber(FacesContext context, UIComponent component,
			Object value) {

		String number = (String) value;

		for (Pet pet : pm.getAllPets()) {
			if (pet.getNumber().equalsIgnoreCase(number)) {
				FacesMessage message = new FacesMessage(
						"Pet with this number already exists in database");
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				throw new ValidatorException(message);
			}
		}
	}

	// Multi field validation with <f:event>
	// Rule: first two digits of number must match last two digits of the year of
	// birth
	public void validateNumberDob(ComponentSystemEvent event) {

		UIForm form = (UIForm) event.getComponent();
		UIInput number = (UIInput) form.findComponent("number");
		UIInput dob = (UIInput) form.findComponent("dob");

		if (number.getValue() != null && dob.getValue() != null
				&& number.getValue().toString().length() >= 2) {
			String twoDigitsOfNumber = number.getValue().toString().substring(0, 2);
			Calendar cal = Calendar.getInstance();
			cal.setTime(((Date) dob.getValue()));

			String lastDigitsOfDob = ((Integer) cal.get(Calendar.YEAR))
					.toString().substring(2);

			if (!twoDigitsOfNumber.equals(lastDigitsOfDob)) {
				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(form.getClientId(), new FacesMessage(
						"Number doesn't match date of birth"));
				context.renderResponse();
			}
		}
	}
}
